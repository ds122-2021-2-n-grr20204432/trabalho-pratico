<?php
require 'db_credentials.php';

// Create connection
$conn = mysqli_connect($servername, $username, $db_password);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create database
$sql = "CREATE DATABASE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database created successfully<br>";
} else {
    echo "<br>Error creating database: " . mysqli_error($conn);
}

// Choose database
$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database changed successfully<br>";
} else {
    echo "<br>Error changing database: " . mysqli_error($conn);
}

// sql to create table_users
$sql = "CREATE TABLE $table_users (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(128) NOT NULL,
  created_at DATETIME,
  updated_at DATETIME,
  last_login_at DATETIME,
  last_logout_at DATETIME,
  UNIQUE (email)
)";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table '" . $table_users . "' created successfully<br>";
} else {
    echo "<br>Error creating database '" . $table_users . "' : " . mysqli_error($conn);
}

// sql to create table_comments
$sql = "CREATE TABLE $table_comments (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL,
    comentario TEXT NOT NULL,
    artigoID INT(6) NOT NULL
  )";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table '" . $table_comments . "' created successfully<br>";
} else {
    echo "<br>Error creating database '" . $table_comments . "' : " . mysqli_error($conn);
}

// sql to create table_artigo
$sql = "CREATE TABLE $table_articles (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(100) NOT NULL,
    texto LONGTEXT NOT NULL
  )";

if (mysqli_query($conn, $sql)) {
    echo "<br>Table '" . $table_articles . "' created successfully<br>";
} else {
    echo "<br>Error creating database '" . $table_articles . "' : " . mysqli_error($conn);
}

// sql to create user admin
$name = "admin";
$email = "admin@a.com";
$password = "admin";
$password = md5($password);

$sql = "INSERT INTO $table_users
    (name, email, password) VALUES
    ('$name', '$email', '$password');";

if (mysqli_query($conn, $sql)) {
    echo "<br>User admin created successfully<br>";
} else {
    echo "<br>Error creating user: " . mysqli_error($conn);
}

mysqli_close($conn)
?>