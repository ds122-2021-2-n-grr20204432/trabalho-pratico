<?php
  require_once "db_credentials.php";
  include "sanitize.php";

  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }

    $form_titulo = $form_texto = "";
    $msg_texto = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['form_titulo']) && isset($_POST['form_texto'])){
        $form_titulo = $_POST['form_titulo'];
        $form_texto = $_POST['form_texto'];
        $form_texto = nl2br($form_texto); 

        $titulo = mysqli_real_escape_string($conn, $form_titulo);
        $texto = mysqli_real_escape_string($conn, $form_texto);
        $titulo = sanitizetxt($titulo);
        $texto = sanitizetxt($texto);

        $sql = "INSERT INTO $table_articles (titulo, texto)
          VALUES ('$titulo', '$texto')";

        if (!mysqli_query($conn, $sql)) {
          die("Error: " . $sql . "<br>" . mysqli_error($conn));
        }
        else {
          $msg_texto = "Artigo salvo com sucesso!";
          header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php");
          exit();
        }
      }	
      elseif(isset($_POST["novo-texto"]) && isset($_POST["id"])){

        $novo_texto = $_POST["novo-texto"];
        $novo_texto = nl2br($novo_texto); 
        $novo_texto = sanitizetxt($novo_texto);
        $id = $_POST["id"];
        $id = sanitize($id);
      
        $sql = "UPDATE $table_articles
            SET texto='". mysqli_real_escape_string($conn, $novo_texto) .
            "' WHERE id=" . mysqli_real_escape_string($conn, $id);

        if(!mysqli_query($conn,$sql)){
          die("Problemas para executar ação no BD!<br>".
            mysqli_error($conn));
        }
        else {
            $msg = "Artigo atualizado com sucesso!";
            header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php#form-anchor" . $id . "texto");
            exit();
        }
      }
    }

    mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Criar Texto</title>
</head>
<body>
  <div class="container">
    <form action="<?= sanitize($_SERVER['PHP_SELF']) ?>" method="post">
      <div class="form-group">  
        <label for="form_titulo">Título:</label><br>
        <input class="form-control" type="text" name="form_titulo" value="<?= $form_titulo ?>" placeholder="Titulo do artigo"><br>
      </div>        
      <div class="form-group">
        <label for="form_texto">Texto:</label><br>
        <textarea class="form-control" name="form_texto" style="width:100%; height:650px;" placeholder="Texto do artigo"><?= $form_texto ?></textarea><br>
      </div>
      <input class="btn btn-default" type="submit" name="submit" value="Enviar">
      <a class="btn btn-default" href="index.php">Voltar</a>
    </form>
  </div>  
</body>
</html>