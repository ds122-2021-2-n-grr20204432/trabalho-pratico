<?php
require 'db_credentials.php';
include 'sanitize.php';

$conn = mysqli_connect($servername,$username,$db_password,$dbname);
if (!$conn) {
  die("Problemas ao conectar com o BD!<br>".
       mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (isset($_GET["id"])) {

    $id = $_GET['id'];
    $id = mysqli_real_escape_string($conn, $id);
    $id = sanitize($id);

    $sql = "SELECT id,comentario,artigoID FROM $table_comments WHERE id = ". $id;

    if(!($comment = mysqli_query($conn,$sql))){
      die("Problemas para carregar tarefas do BD!<br>".
           mysqli_error($conn));
    }
  }
}
mysqli_close($conn);
if (mysqli_num_rows($comment) != 1) {
    die("Id de tarefa incorreto.");
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Editar Comentário</title>
</head>
<body>
<div class="container">
<form action="index.php" method="POST">
        <div class="form-group">
          <?php $comment = mysqli_fetch_assoc($comment); ?>
          <input type="hidden" name="artigoID" value="<?php echo $comment["artigoID"] ?>">
          <input type="hidden" name="id" value="<?php echo $comment["id"] ?>">
          <label>Editar Comentário</label><br>
          <textarea required name="novo-comentario" rows="8" cols="80" placeholder="Novo comentário"><?php echo $comment["comentario"]; ?></textarea><br>
          <input class="btn btn-default" type="submit" name="submit" value="Enviar">
          <a class="btn btn-default" href="index.php">Voltar</a>
        </div>
      </form>
</div>     
</body>
</html>