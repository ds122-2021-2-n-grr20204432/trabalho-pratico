<?php
require_once 'db_credentials.php';
include 'sanitize.php';

$conn = mysqli_connect($servername,$username,$db_password,$dbname);
if (!$conn) {
  die("Problemas ao conectar com o BD!<br>".
       mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (isset($_GET["id"])) {

    $id = $_GET['id'];
    $id = mysqli_real_escape_string($conn, $id);
    $id = sanitize($id);

    $sql = "SELECT id,texto FROM $table_articles WHERE id = ". $id;

    if(!($artigo = mysqli_query($conn,$sql))){
      die("Problemas para carregar tarefas do BD!<br>".
           mysqli_error($conn));
    }
  }
}
mysqli_close($conn);
if (mysqli_num_rows($artigo) != 1) {
    die("Id de artigo incorreto.");
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Editar Artigo</title>
</head>
<body>
<div class="container">
<form action="cria_texto.php" method="POST">
        <div class="form-group">
          <?php $artigo = mysqli_fetch_assoc($artigo); ?>
          <input class="form-control" type="hidden" name="id" value="<?php echo $artigo["id"] ?>">
          <label>Editar Texto</label>
          <textarea required class="form-control" name="novo-texto" style="width:100%; height:650px;" placeholder="Texto do artigo"><?php echo str_replace("<br />", "", $artigo["texto"]); ?></textarea><br>
          <input class="btn btn-default" type="submit" name="submit" value="Enviar">
          <a class="btn btn-default" href="index.php">Voltar</a>
        </div>
      </form>
</div>
</body>
</html>